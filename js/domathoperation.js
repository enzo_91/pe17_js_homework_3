function doMathOperation(numberA, numberB, operator) {
    switch (operator) {
        case '+':
            return numberA + numberB;
        case '-':
            return numberA - numberB;
        case '*':
            return numberA * numberB;
        case '/':
            return numberA / numberB;
        default:
            return NaN;
    }
}

let numberA = null;
let numberB = null;
let result = null;

do {
    numberA = Number(prompt('введите пожалуйста первое число'));
} while(typeof numberA !== 'number');

do {
    numberB = Number(prompt('введите пожалуйста второе число'));
} while(typeof numberB !== 'number');

do {
    let operator = prompt("введите пожалуйста математическую операцию '+', '-', '/', '*'");

    result = doMathOperation(numberA, numberB, operator);
} while(isNaN(result));

alert(result);